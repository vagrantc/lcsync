# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.1] - 2022-05-19

### Added
- this CHANGELOG

### Changed
- Makefile: Pass -ffile-prefix-map in CFLAGS to avoid embedding the build path.
- Makefile: remove unused dependencies
- Makefile: don't hardcode bash path
- updated COPYING: more explicit license wording
- fix SPDX in header files
- updated README: better description + document options

### Fixed
- various license headers
- applied patches for Debian packaging

## [0.0.0] - 2022-05-16

- Initial release.
