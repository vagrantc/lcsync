/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2022 Brett Sheffield <bacs@librecast.net> */

#ifndef _NET_PRIV_H
#define _NET_PRIV_H 1

#define NET_DEBUG 1

#include "net.h"

#endif /* _NET_PRIV_H */
